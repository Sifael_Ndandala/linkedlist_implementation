"""

This is an implementation of the LinkedList Data Structure.
A linked list is connected a datastructure with nodes containg
data and pointer to the next node.

We implement two classes:

-- Node Class with data and pointer
-- LinkedList class with head and tail objects

Below we implement the following methods:

Methods

-- Append a node to the linkedlist
-- Return the length of a linkedlist
-- Print the LinkedList
-- Prepend Node at the head of list
-- Insert at Node with index
-- Insert at Node with value
-- Get data at node index
-- Delete node at index
-- Delete node at value

"""

class Node:

    def __init__(self, data=None):
        self.data = data 
        self.pointer = None

    def __str__(self):
        return f"{self.data}"


class LinkedList: 

    def __init__(self):
        self.head = None 
        self.tail = None 


    def append_node(self, node):
        if not isinstance(node, Node):
            node = Node(node)

        if not self.head:
            self.head = node
        else:
            self.tail.pointer = node 
        
        self.tail = node


    def prepend_node(self, node):
        if not isinstance(node, Node):
            node = Node(node)

        current_node = self.head
        if current_node == None:
            self.append_node(node)
            return f"{node} appended"
        else:
            node.pointer = self.head
            self.head = node 

    
    def insert_at_index(self, node, index):
        if not isinstance(node, Node):
            node = Node(node)

        if index > self.size()-1 or index < 0:
            return f"{index} is out of range"

        if index == 0:
            self.prepend_node(node)
            return f"{node} has been prepended"
        
        current_node = self.head
        list_index = 1

        while current_node:
            if list_index == index:
                node.pointer = current_node.pointer
                current_node.pointer = node
                return 

            current_node = current_node.pointer
            list_index += 1
        

    
    def insert_at_node(self, pre_node, node):
        if not isinstance(node, Node):
            node = Node(node)

        current_node = self.head
        while current_node:
            if current_node.data == pre_node:
                node.pointer = current_node.pointer
                current_node.pointer = node 
                return
            current_node = current_node.pointer

        return f"{pre_node} is not in the list"


    def get_value(self, index):
        if index > self.size()-1 or index < 0:
            return f"{index} is out of range"

        current_node = self.head
        list_index = 0

        while current_node:
            if list_index == index:
                return current_node.data 
            else:
                current_node = current_node.pointer
                list_index += 1
            
    
    def delete_at_index(self, index):
        if index > self.size()-1 or index < 0:
            return f"{index} is out of range"

        # Delete head 
        current_node = self.head
        list_index = 1

        if index == 0:
            self.head = current_node.pointer
            current_node = None 
            return
        
        while current_node:
            prev_node = current_node
            next_node = current_node.pointer
            if list_index == index:
                prev_node.pointer = next_node
                current_node.pointer = next_node.pointer
                return 
            else:
                current_node = current_node.pointer
                list_index += 1
                 

    def delete_node(self, value):
        current_node = self.head
        
        if value == self.head.data:
            self.head = current_node.pointer
            current_node = None
            return 

        while current_node and current_node.data != value:
            prev_node = current_node
            current_node = current_node.pointer

        if current_node is None:
            return f"{value} not in the LinkedList"

        prev_node.pointer = current_node.pointer
        current_node = None

    def __str__(self):
        current_node = self.head
        linked_list = ""
        while current_node:
            linked_list += str(current_node.data) + " -> "
            current_node = current_node.pointer

        if linked_list:
            return linked_list[:-3]
        else:
            return f""

    
    def size(self):
        current_node = self.head
        size = 0
        while current_node:
            size += 1
            current_node = current_node.pointer

        return size

    

# Sample List
sample_list = LinkedList()
print(sample_list)
print(sample_list.size())

# Append Examples
sample_list.append_node("George")
sample_list.append_node("Billie")

print(sample_list)
print(sample_list.size())


# Prepend Examples
sample_list.prepend_node("Kani")
print(sample_list)
print(sample_list.size())

# Insert at Index
sample_list.insert_at_index("London", 1)
sample_list.insert_at_index("Jeremy", 4)
print(sample_list)
print(sample_list.size())

# Insert After Node
sample_list.insert_at_node("London", "Nairobi")
print(sample_list)
print(sample_list.size())

# Return data at index
print(sample_list.get_value(0))
print(sample_list.get_value(3))

# Delete Node at Index
print(sample_list)
sample_list.delete_at_index(1)
sample_list.delete_at_index(2)
print(sample_list)

#  Delete Node at Value
sample_list.delete_node("Kani")
print(sample_list)


